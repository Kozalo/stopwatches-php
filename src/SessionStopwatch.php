<?php
namespace ru\kozalo\stopwatches;

use ru\kozalo\stopwatches\exceptions\AlreadyStartedStopwatchException;
use ru\kozalo\stopwatches\exceptions\NoStartedStopwatchException;


/**
 * Class SessionStopwatch
 *
 * This class extends the functionality of the Stopwatch.
 * It uses PHP SESSION to store an array of several measurements.
 * It may be useful if you want to analyze multiple measurements later.
 *
 * @author Leonid Kozarin <kozalo@nekochan.ru>
 * @copyright Kozalo.Ru, 2017
 * @license MIT
 * @package ru\kozalo\stopwatches
 * @uses Stopwatch, AlreadyStartedStopwatchException, NoStartedStopwatchException
 */
class SessionStopwatch
{
    private $name;                  // the name of a measurement set
    private $stopwatch;             // ru\kozalo\Stopwatch
    private $information = [];      // data to store
    private $lastDifference = 0;    // used to calculate elapsed time for each measurement

    const SESSION_KEY = 'ru\kozalo\stopwatches\SessionStopwatch\elapsed_time_information';


    /**
     * SessionStopwatch constructor.
     * @param string $name A name of a measurement set.
     */
    public function __construct($name)
    {
        $this->name = $name;
        $this->stopwatch = new Stopwatch();

        if (session_status() == PHP_SESSION_NONE)
            session_start();
    }


    /**
     * Start
     *
     * Starts the stopwatch from zero.
     *
     * @param string $label A label for a measurement.
     * @throws AlreadyStartedStopwatchException
     */
    public function Start($label)
    {
        $this->lastDifference = 0;
        $this->information = [];

        $this->information[] = [
            'label' => $label,
            'start_time' => $this->stopwatch->Start()
        ];
    }


    /**
     * GoOn
     *
     * Continues the stopwatch.
     *
     * @param string $label A label for a measurement.
     * @throws AlreadyStartedStopwatchException
     */
    public function GoOn($label)
    {
        $this->information[] = [
            'label' => $label,
            'start_time' => $this->stopwatch->GoOn()
        ];
    }


    /**
     * Stop
     *
     * Stops the stopwatch.
     *
     * @throws NoStartedStopwatchException
     */
    public function Stop()
    {
        $currentElement = &$this->information[count($this->information)-1];
        $totalDifference = $this->stopwatch->Stop();
        $difference = $totalDifference - $this->lastDifference;

        $currentElement['stop_time'] = $currentElement['start_time'] + $difference;
        $currentElement['elapsed_time'] = $difference;
        $currentElement['total_elapsed_time'] = $totalDifference;

        $this->lastDifference = $totalDifference;
    }


    /**
     * IsRunning
     *
     * Returns *true* if the stopwatch is working and *false* otherwise.
     *
     * @return bool
     */
    public function IsRunning()
    {
        return $this->stopwatch->IsRunning();
    }


    /**
     * SaveToSession
     *
     * Only after you call this method, all measurement data will be saved in the SESSION.
     * If the stopwatch is running, it will be stopped automatically.
     */
    public function SaveToSession()
    {
        if ($this->IsRunning())
            $this->Stop();

        $this->information['total_elapsed_time'] = $this->lastDifference;

        if (!array_key_exists(self::SESSION_KEY, $_SESSION))
            $_SESSION[self::SESSION_KEY] = [
                $this->name => []
            ];
        else if (!array_key_exists($this->name, $_SESSION[self::SESSION_KEY]))
            $_SESSION[self::SESSION_KEY][$this->name] = [];

        $_SESSION[self::SESSION_KEY][$this->name][] = [
            'request_time' => date('d.m.Y H:i:s'),
            'timings' => $this->information
        ];
    }


    /**
     * GetInformationFromSession
     *
     * A static method to get the data back from the SESSION.
     *
     * @return array|null
     */
    public static function GetInformationFromSession()
    {
        if (session_status() == PHP_SESSION_NONE)
            session_start();

        if (!array_key_exists(self::SESSION_KEY, $_SESSION))
            return null;

        $value = $_SESSION[self::SESSION_KEY];
        return (!empty($value)) ? $value : null;
    }
}
