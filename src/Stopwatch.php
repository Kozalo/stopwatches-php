<?php
namespace ru\kozalo\stopwatches;

use ru\kozalo\stopwatches\exceptions\AlreadyStartedStopwatchException;
use ru\kozalo\stopwatches\exceptions\NoStartedStopwatchException;


/**
 * Class Stopwatch
 *
 * A simple stopwatch to measure elapsed time.
 *
 * @author Leonid Kozarin <kozalo@nekochan.ru>
 * @copyright Kozalo.Ru, 2017
 * @license MIT
 * @package ru\kozalo\stopwatches
 * @uses AlreadyStartedStopwatchException, NoStartedStopwatchException
 */
class Stopwatch
{
    private $startTime;
    private $totalDiffTime;
    private $started = false;


    /**
     * Start
     *
     * Starts the stopwatch from the zero.
     *
     * @return float The current timestamp.
     * @throws AlreadyStartedStopwatchException
     */
    public function Start()
    {
        $this->totalDiffTime = 0;
        return $this->GoOn();
    }


    /**
     * Stop
     *
     * Stops the stopwatch.
     *
     * @return float The amount of elapsed time.
     * @throws NoStartedStopwatchException
     */
    public function Stop()
    {
        if (!$this->started)
            throw new NoStartedStopwatchException();

        $this->totalDiffTime += microtime(true) - $this->startTime;
        $this->started = false;
        return $this->totalDiffTime;
    }


    /**
     * GoOn
     *
     * Continues the stopwatch.
     *
     * @return float The current timestamp.
     * @throws AlreadyStartedStopwatchException
     */
    public function GoOn()
    {
        if ($this->started)
            throw new AlreadyStartedStopwatchException();

        $this->startTime = microtime(true);
        $this->started = true;
        return $this->startTime;
    }


    /**
     * IsRunning
     *
     * Returns *true* if the stopwatch is working and *false* otherwise.
     *
     * @return bool
     */
    public function IsRunning()
    {
        return $this->started;
    }
}
