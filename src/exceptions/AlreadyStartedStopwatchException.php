<?php
namespace ru\kozalo\stopwatches\exceptions;

use Exception;


/**
 * Class AlreadyStartedStopwatchException
 *
 * It will be thrown if you try to start or continue an already running stopwatch.
 *
 * @package ru\kozalo\stopwatches\exceptions
 */
class AlreadyStartedStopwatchException extends Exception {}
