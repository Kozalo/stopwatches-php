<?php
namespace ru\kozalo\stopwatches\exceptions;

use Exception;


/**
 * Class NoStartedStopwatchException
 *
 * It will be thrown if you try to stop a non-running stopwatch.
 *
 * @package ru\kozalo\stopwatches\exceptions
 */
class NoStartedStopwatchException extends Exception {}
