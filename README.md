Stopwatches
==============================

Для русскоговорящих
-------------------

Данный набор содержит набор из нескольких секундомеров для замера времени выполнения (хотя и не только) Ваших скриптов.  

### Stopwatch

Базовый класс, предоставляющий возможность запуска секундомера (`Start()`), остановки (`Stop()`) и возобновления (`GoOn()`).  
`Start()` отличается от `GoOn()` тем, что обнуляет счётчик времени. При последовательности остановок и продолжений отсчёта
итоговое время будет складываться. А так они оба при вызове возвращают *timestamp* времени запуска секундомера.  
Ещё есть метод `IsRunning()`, который, как очевидно из названия, возвращает *true*, если отсчёт запущен, и *false* в противом случае.  


### SessionStopwatch

Надстройка над стандартным секундомером. Используя его, реализует хранение наборов данных в PHP-сессии.  
Имеет те же методы, что и базовый класс, но конструктор и методы запуска и возобновления принимают в качестве параметра
строку-метку. Строка из конструктора используется в качестве ключа массива и названия замера. Строки в методах запуска
используются для осмысленного наименования различных частей замера.  
Это может быть полезно, например, при замере времени выполнения разных частей загрузки веб-страницы. Для этого создаётся
объект сессионного секундомера с названием страницы, далее повторяем запуски/возобновления и остановки отсчёта,
оборачивая каждый блок кода (при этом каждому блоку даём осмысленную метку), а в конце замера вызываем метод `SaveToSession()`,
чтобы сохранить результат замеров в сессию. При этом новые результаты не перезаписывают старые, а добавляются новыми элементами.
Например, у массива может быть ключ `'Главная страница'` под которым размещается массив из нумерованных элементов-замеров.
У каждого замера будет поле `request_time`, содержащее строковое представление времени выполнения замера, а также массив
`timings`, элементами которого будут различные этапы одного замера (а кроме того, ещё и элемент `total_elapsed_time`,
значение которого означает общее время выполнения всех этапов). Структура каждого этапа такова:  

* `label` *(string)* — метка, указанная при запуске замера.  
* `start_time` *(float)* — время начала замера; *timestamp* с микросекундами.  
* `stop_time` *(float)* — аналогично предыдущему, но время остановки замера.  
* `elapsed_time` *(float)* — время в микросекундах, затраченное на выполнение данного шага.  
* `total_elapsed_time` *(float)* — аналогично предыдущему, но плюсуется ко времени выполнения предыдущих шагов.  

Результат замеров можно получить с помощью статического метода `GetInformationFromSession()`. Пример распечатки одного
из моих замеров, распечатанного с помощью функции `print_r()` находится в файле `result.example`.  


### Исключения

Во время выполнения замеров могут выбрасываться некоторые исключения:  

* `AlreadyStartedStopwatchException` — выбросится, если Вы попытаетесь запустить уже запущенный секундомер.  
* `NoStartedStopwatchException` — выбросится, если Вы попытаетесь остановить незапущенный секундомер.  



In English
----------

### Stopwatch

It's a basic class for measuring elapsed time. It has the following methods listed below:  

* `Start()` — starts a new measuring from zero and returns a *timestamp* describing the moment of time when you call it.  
* `Stop()` — stops the stopwatch and returns the total amount of elapsed time.  
* `GoOn()` — starts the measuring from the time it was stopped.  
* `IsRunning()` — return *true* if the stopwatch is running and *false* otherwise.  


### SessionStopwatch

This class extends the functionality of the `Stopwatch`. It uses PHP SESSION to store an array of several measurements.
It may be useful if you want to analyze multiple measurements later on.  
It has the same methods as `Stopwatch` does but with some difference:  

* `__construct($name)` — the constructor takes the name of a measurement. It's used as a key for the array.  
* `Start($label)` and `GoOn($label)` — these methods takes strings to label the blocks of code where it's used.  

The structure of a result array will be as the following example:  

* Constructor Name:  
    * \[0]:  
        * `request_time` — the time of performing the measurement.  
        * `timings`:  
            * \[0]:  
                * `label`  
                * `start_time` — the time in microseconds when the block of the measurement was started.  
                * `stop_time` — the time in microseconds when the block of the measurement was stopped.  
                * `elapsed_time` — the amount of time in microseconds which was spent.  
                * `total_elapsed_time` — the total amount of time in microseconds which was spent for executing this and all previous blocks.  
            * \[1]: ...
            * `total_elapsed_time` — the total amount of time in microseconds which was spent for executing all blocks.  
    * \[1]: ...
* Another Constructor Name: ...  

I strongly suggest you examine an example of my measurement. See the file `result.example` for it.  

Also, it adds two new methods for working with the SESSION:  

* `SaveToSession()` — only after you call this method, all measurement data will be saved in the SESSION.  
* `GetInformationFromSession()` — a static method to let you get the results back.  


### Exceptions

There are two exceptions you may encounter while working with the library.  

* `AlreadyStartedStopwatchException` — it will be thrown if you try to start or continue an already running stopwatch.  
* `NoStartedStopwatchException` — it will be thrown if you try to stop a non-running stopwatch.
